<?php
/*
Plugin Name: Custom Plugin
Plugin URI: https://www.7learn.com
Description:  یک پلاگین سفارشی ساده
Author: Kaivan Alimohammadi<keivan.amohamadi@gmail.com>
Version: 1.2.3
Author URI: https://www.7learn.com
*/
include "new_plugin_function.php";
add_action('admin_menu','add_sample_menu_page');

function add_sample_menu_page()
{
	add_menu_page('صفحه مدیریت','صفحه مدیریت','manage_options','sample_menu','sample_admin_page_content');
}
function send_welcome_email( $id, $email ) {
	//print "send email for user with id :{$id} => {$email}";
}

function send_welcome_sms( $id, $email ) {
	//print "send sms for user with id :{$id} => $email";
}

add_action( 'new_user_register', function ( $id, $email ) {

}, 1, 2 );
add_action( 'new_user_register', 'send_welcome_sms', 2, 2 );
$user_id    = 25;
$user_email = 'info@7learn.com';
do_action( 'new_user_register', $user_id, $user_email );

//filter hook

$number = 10;
add_filter( 'calculate_number', function ( $number ) {
	return $number + 2;
} );
add_filter( 'calculate_number', function ( $number ) {
	return $number + 5;
} );
$result = apply_filters( 'calculate_number', $number );

$userData = [
	'first_name' => 'mehrdad',
	'last_name'  => 'azimi',
	'age'        => 26
];
add_filter('change_user_data',function($data){
	$data['first_name'] = strtoupper($data['first_name']);
	return $data;
},2);
add_filter('change_user_data',function($data){
	$data['last_name'] = strtoupper($data['last_name']);
	$data['first_name'] = strtolower($data['first_name']);
	return $data;
},1);
$resultData = apply_filters( 'change_user_data',$userData );
//var_dump($resultData);

function remove_hello_words($content)
{
	$content = preg_replace('/سلام/','',$content);
	return $content;
}
function words_link($content){
	$words = [
		'تصادفی'  => 'https://www.7learn.com/tag/تصادفی',
		'مشاهده'   => 'https://www.7learn.com/tag/مشاهده'
	];
	foreach ($words as $word => $url)
	{
		$link = "<a href='{$url}'>{$word}</a>";
		$content = preg_replace("/$word/",$link,$content);
	}
	return $content;
}
function limit_content($content)
{
	$hasVipAccount = userHasVipAccount(2346);
	$readMore = "<a href=''>خرید اکانت ویژه</a>";
	if($hasVipAccount)
	{
		$readMore = "<a href=''>ادامه</a>";
	}
	return wp_trim_words($content,10).$readMore;
}
add_filter('content','remove_hello_words',1);
add_filter('content','words_link',2);
add_filter('content','limit_content',3);

function add_section_one(){
	include "section_one.php";
}
function add_section_two()
{
	include "section_two.php";
}

add_action('sample_page_contents','add_section_one');
add_action('sample_page_contents','add_section_two');

function sample_admin_page_content()
{
	$content= "سلام این فقط یک افزونه نیست، این نشان‌دهنده امید و شور و شوق یک نسل به طور خلاصه در دو کلمه است که توسط فرد مشهور، لوئیس آرمسترانگ سروده شده است: سلام، عزیزم. هنگامی که شما آن را فعال کنید، به صورت تصادفی یک متن از آهنگ سلام، عزیزم در گوشه چپ هر صفحه در بخش مدیریت مشاهده می‌کنید.";

	$result = apply_filters('content',$content);
	//echo $result;

	do_action('sample_page_contents');


}
add_filter('show_admin_bar','__return_false');
add_filter('show_admin_bar','__return_true');