<?php die(); ?><!DOCTYPE html>
<html dir="rtl" lang="fa-IR" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="http://woo-experts.local/xmlrpc.php">
	<!--[if lt IE 9]>
	<script src="http://woo-experts.local/wp-content/themes/twentyfifteen/js/html5.js"></script>
	<![endif]-->
	<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>
<title>دوره متخصص وردپرس و ووکامرس سون لرن &#8211; متخصص وردپرس</title>
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link href='https://fonts.gstatic.com' crossorigin rel='preconnect' />
<link rel="alternate" type="application/rss+xml" title="دوره متخصص وردپرس و ووکامرس سون لرن &raquo; خوراک" href="http://woo-experts.local/feed/" />
<link rel="alternate" type="application/rss+xml" title="دوره متخصص وردپرس و ووکامرس سون لرن &raquo; خوراک دیدگاه‌ها" href="http://woo-experts.local/comments/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/woo-experts.local\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.6"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56692,8205,9792,65039],[55357,56692,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='dashicons-css'  href='http://woo-experts.local/wp-includes/css/dashicons.min.css?ver=4.9.6' type='text/css' media='all' />
<link rel='stylesheet' id='admin-bar-rtl-css'  href='http://woo-experts.local/wp-includes/css/admin-bar-rtl.min.css?ver=4.9.6' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='http://woo-experts.local/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.2' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-rtl-css'  href='http://woo-experts.local/wp-content/plugins/contact-form-7/includes/css/styles-rtl.css?ver=5.0.2' type='text/css' media='all' />
<link rel='stylesheet' id='twentyfifteen-fonts-css'  href='https://fonts.googleapis.com/css?family=Noto+Sans%3A400italic%2C700italic%2C400%2C700&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />
<link rel='stylesheet' id='genericons-css'  href='http://woo-experts.local/wp-content/themes/twentyfifteen/genericons/genericons.css?ver=3.2' type='text/css' media='all' />
<link rel='stylesheet' id='twentyfifteen-style-css'  href='http://woo-experts.local/wp-content/themes/twentyfifteen/style.css?ver=4.9.6' type='text/css' media='all' />
<!--[if lt IE 9]>
<link rel='stylesheet' id='twentyfifteen-ie-css'  href='http://woo-experts.local/wp-content/themes/twentyfifteen/css/ie.css?ver=20141010' type='text/css' media='all' />
<![endif]-->
<!--[if lt IE 8]>
<link rel='stylesheet' id='twentyfifteen-ie7-css'  href='http://woo-experts.local/wp-content/themes/twentyfifteen/css/ie7.css?ver=20141010' type='text/css' media='all' />
<![endif]-->
<script type='text/javascript' src='http://woo-experts.local/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='http://woo-experts.local/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<link rel='https://api.w.org/' href='http://woo-experts.local/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://woo-experts.local/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://woo-experts.local/wp-includes/wlwmanifest.xml" /> 
<link rel="stylesheet" href="http://woo-experts.local/wp-content/themes/twentyfifteen/rtl.css" type="text/css" media="screen" /><meta name="generator" content="WordPress 4.9.6" />
		<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
		<style type="text/css" media="print">#wpadminbar { display:none; }</style>
<style type="text/css" media="screen">
	html { margin-top: 32px !important; }
	* html body { margin-top: 32px !important; }
	@media screen and ( max-width: 782px ) {
		html { margin-top: 46px !important; }
		* html body { margin-top: 46px !important; }
	}
</style>
</head>

<body class="rtl home blog logged-in admin-bar no-customize-support">
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content">رفتن به نوشته‌ها</a>

	<div id="sidebar" class="sidebar">
		<header id="masthead" class="site-header" role="banner">
			<div class="site-branding">
										<h1 class="site-title"><a href="http://woo-experts.local/" rel="home">دوره متخصص وردپرس و ووکامرس سون لرن</a></h1>
											<p class="site-description">متخصص وردپرس</p>
									<button class="secondary-toggle">فهرست و ابزارک‌ها</button>
			</div><!-- .site-branding -->
		</header><!-- .site-header -->

			<div id="secondary" class="secondary">

					<nav id="site-navigation" class="main-navigation" role="navigation">
				<div class="menu-%d9%85%d9%86%d9%88%db%8c-%d8%a7%d8%b5%d9%84%db%8c-container"><ul id="menu-%d9%85%d9%86%d9%88%db%8c-%d8%a7%d8%b5%d9%84%db%8c" class="nav-menu"><li id="menu-item-17" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-17"><a href="http://woo-experts.local/category/%d8%a2%d9%85%d9%88%d8%b2%d8%b4/">آموزش</a></li>
<li id="menu-item-19" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-19"><a href="http://woo-experts.local/%d8%aa%d9%85%d8%a7%d8%b3-%d8%a8%d8%a7-%d9%85%d8%a7/">تماس با ما</a></li>
<li id="menu-item-18" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-18"><a href="http://woo-experts.local/%d8%a7%d9%88%d9%84%db%8c%d9%86-%d9%85%d8%b7%d9%84%d8%a8-%d9%85%d9%86-%d8%af%d8%b1-%d9%88%d8%b1%d8%af%d9%be%d8%b1%d8%b3/">اولین مطلب من در وردپرس</a></li>
<li id="menu-item-20" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-20"><a href="http://woo-experts.local/%d9%88%d8%a7%d8%ad%d8%af-%d8%a8%d8%a7%d8%b2%d8%b1%d8%b3%db%8c/">واحد بازرسی</a>
<ul class="sub-menu">
	<li id="menu-item-21" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-21"><a href="http://woo-experts.local/%d9%88%d8%a7%d8%ad%d8%af-%d8%a8%d8%a7%d8%b2%d8%b1%d8%b3%db%8c/%d9%88%d8%a7%d8%ad%d8%af-%d8%a8%d8%a7%d8%b2%d8%b1%d8%b3%db%8c-2/">واحد بازرسی 2</a></li>
</ul>
</li>
</ul></div>			</nav><!-- .main-navigation -->
		
		
					<div id="widget-area" class="widget-area" role="complementary">
				<aside id="recent-comments-2" class="widget widget_recent_comments"><h2 class="widget-title">آخرین دیدگاه‌ها</h2><ul id="recentcomments"><li class="recentcomments"><span class="comment-author-link">alimohammadi</span> در <a href="http://woo-experts.local/%d8%a7%d9%88%d9%84%db%8c%d9%86-%d9%85%d8%b7%d9%84%d8%a8-%d9%85%d9%86-%d8%af%d8%b1-%d9%88%d8%b1%d8%af%d9%be%d8%b1%d8%b3/#comment-3">اولین مطلب من در وردپرس</a></li><li class="recentcomments"><span class="comment-author-link">alimohammadi</span> در <a href="http://woo-experts.local/%d8%a7%d9%88%d9%84%db%8c%d9%86-%d9%85%d8%b7%d9%84%d8%a8-%d9%85%d9%86-%d8%af%d8%b1-%d9%88%d8%b1%d8%af%d9%be%d8%b1%d8%b3/#comment-2">اولین مطلب من در وردپرس</a></li><li class="recentcomments"><span class="comment-author-link"><a href='https://wordpress.org/' rel='external nofollow' class='url'>یک نویسنده‌ی دیدگاه در وردپرس</a></span> در <a href="http://woo-experts.local/%d8%b3%d9%84%d8%a7%d9%85-%d8%af%d9%86%db%8c%d8%a7/#comment-1">سلام دنیا!</a></li></ul></aside><aside id="search-2" class="widget widget_search"><form role="search" method="get" class="search-form" action="http://woo-experts.local/">
				<label>
					<span class="screen-reader-text">جستجو برای:</span>
					<input type="search" class="search-field" placeholder="جستجو &hellip;" value="" name="s" />
				</label>
				<input type="submit" class="search-submit screen-reader-text" value="جستجو" />
			</form></aside>		<aside id="recent-posts-2" class="widget widget_recent_entries">		<h2 class="widget-title">نوشته‌های تازه</h2>		<ul>
											<li>
					<a href="http://woo-experts.local/%d8%a7%d9%88%d9%84%db%8c%d9%86-%d9%85%d8%b7%d9%84%d8%a8-%d9%85%d9%86-%d8%af%d8%b1-%d9%88%d8%b1%d8%af%d9%be%d8%b1%d8%b3/">اولین مطلب من در وردپرس</a>
									</li>
											<li>
					<a href="http://woo-experts.local/%d8%b3%d9%84%d8%a7%d9%85-%d8%af%d9%86%db%8c%d8%a7/">سلام دنیا!</a>
									</li>
					</ul>
		</aside><aside id="archives-2" class="widget widget_archive"><h2 class="widget-title">بایگانی</h2>		<ul>
			<li><a href='http://woo-experts.local/2018/05/'>می 2018</a></li>
		</ul>
		</aside><aside id="categories-2" class="widget widget_categories"><h2 class="widget-title">دسته‌ها</h2>		<ul>
	<li class="cat-item cat-item-2"><a href="http://woo-experts.local/category/%d8%a2%d9%85%d9%88%d8%b2%d8%b4/" >آموزش</a>
</li>
	<li class="cat-item cat-item-1"><a href="http://woo-experts.local/category/%d8%af%d8%b3%d8%aa%d9%87%e2%80%8c%d8%a8%d9%86%d8%af%db%8c-%d9%86%d8%b4%d8%af%d9%87/" >دسته‌بندی نشده</a>
</li>
		</ul>
</aside><aside id="meta-2" class="widget widget_meta"><h2 class="widget-title">اطلاعات</h2>			<ul>
			<li><a href="http://woo-experts.local/wp-admin/">مدیر وبلاگ</a></li>			<li><a href="http://woo-experts.local/wp-login.php?action=logout&#038;_wpnonce=32ade9493a">بیرون رفتن</a></li>
			<li><a href="http://woo-experts.local/feed/">پیگیری نوشته‌ها با<abbr title="Really Simple Syndication">RSS</abbr></a></li>
			<li><a href="http://woo-experts.local/comments/feed/">پیگیری دیدگاه‌ها با <abbr title="Really Simple Syndication">RSS</abbr></a></li>
			<li><a href="https://wordpress.org/" title="با نیروی وردپرس ، بهترین ابزار وبلاگنویسی جهان">WordPress.org</a></li>			</ul>
			</aside>			</div><!-- .widget-area -->
		
	</div><!-- .secondary -->

	</div><!-- .sidebar -->

	<div id="content" class="site-content">

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		
			
			
<article id="post-4" class="post-4 post type-post status-publish format-standard hentry category-2 tag-4">
	
	<header class="entry-header">
		<h2 class="entry-title"><a href="http://woo-experts.local/%d8%a7%d9%88%d9%84%db%8c%d9%86-%d9%85%d8%b7%d9%84%d8%a8-%d9%85%d9%86-%d8%af%d8%b1-%d9%88%d8%b1%d8%af%d9%be%d8%b1%d8%b3/" rel="bookmark">اولین مطلب من در وردپرس</a></h2>	</header><!-- .entry-header -->

	<div class="entry-content">
		<p>اولین مطلب من در وردپرساولین مطلب من در وردپرساولین مطلب من در وردپرساولین مطلب من در وردپرساولین مطلب من در وردپرساولین مطلب من در وردپرساولین مطلب من در وردپرساولین مطلب من در وردپرساولین مطلب من در وردپرساولین مطلب من در وردپرساولین مطلب من در وردپرساولین مطلب من در وردپرساولین مطلب من در وردپرساولین مطلب من در وردپرساولین مطلب من در</p>
<p><img class="alignnone size-medium wp-image-7" src="http://woo-experts.local/wp-content/uploads/2018/05/Sketch-300x169.png" alt="" width="300" height="169" srcset="http://woo-experts.local/wp-content/uploads/2018/05/Sketch-300x169.png 300w, http://woo-experts.local/wp-content/uploads/2018/05/Sketch-768x432.png 768w, http://woo-experts.local/wp-content/uploads/2018/05/Sketch-1024x576.png 1024w, http://woo-experts.local/wp-content/uploads/2018/05/Sketch.png 1366w" sizes="(max-width: 300px) 100vw, 300px" /></p>
<p>وردپرساولین مطلب من در وردپرساولین مطلب من در وردپرساولین مطلب من در وردپرساولین مطلب من در وردپرساولین مطلب من در وردپرساولین مطلب من در وردپرساولین مطلب من در وردپرساولین مطلب من در وردپرساولین مطلب من در وردپرساولین مطلب من در وردپرساولین مطلب من در وردپرساولین مطلب من در وردپرس</p>
	</div><!-- .entry-content -->

	
	<footer class="entry-footer">
		<span class="posted-on"><span class="screen-reader-text">ارسال شده در </span><a href="http://woo-experts.local/%d8%a7%d9%88%d9%84%db%8c%d9%86-%d9%85%d8%b7%d9%84%d8%a8-%d9%85%d9%86-%d8%af%d8%b1-%d9%88%d8%b1%d8%af%d9%be%d8%b1%d8%b3/" rel="bookmark"><time class="entry-date published" datetime="2018-05-16T15:55:07+00:00">می 16, 2018</time><time class="updated" datetime="2018-05-16T16:03:51+00:00">می 16, 2018</time></a></span><span class="cat-links"><span class="screen-reader-text">دسته‌ها </span><a href="http://woo-experts.local/category/%d8%a2%d9%85%d9%88%d8%b2%d8%b4/" rel="category tag">آموزش</a></span><span class="tags-links"><span class="screen-reader-text">برچسب‌ها </span><a href="http://woo-experts.local/tag/%d8%b3%db%8c%d8%a7%d8%b3%db%8c/" rel="tag">سیاسی</a></span><span class="comments-link"><a href="http://woo-experts.local/%d8%a7%d9%88%d9%84%db%8c%d9%86-%d9%85%d8%b7%d9%84%d8%a8-%d9%85%d9%86-%d8%af%d8%b1-%d9%88%d8%b1%d8%af%d9%be%d8%b1%d8%b3/#comments">2 دیدگاه <span class="screen-reader-text">برای اولین مطلب من در وردپرس</span></a></span>		<span class="edit-link"><a class="post-edit-link" href="http://woo-experts.local/wp-admin/post.php?post=4&#038;action=edit">ویرایش</a></span>	</footer><!-- .entry-footer -->

</article><!-- #post-## -->

<article id="post-1" class="post-1 post type-post status-publish format-standard hentry category-1">
	
	<header class="entry-header">
		<h2 class="entry-title"><a href="http://woo-experts.local/%d8%b3%d9%84%d8%a7%d9%85-%d8%af%d9%86%db%8c%d8%a7/" rel="bookmark">سلام دنیا!</a></h2>	</header><!-- .entry-header -->

	<div class="entry-content">
		<p>به وردپرس فارسی خوش آمدید.‌ این نخستین نوشته‌‌ی شماست. می‌توانید ویرایش یا پاکش کنید و پس از آن نوشتن را آغاز کنید!</p>
	</div><!-- .entry-content -->

	
	<footer class="entry-footer">
		<span class="posted-on"><span class="screen-reader-text">ارسال شده در </span><a href="http://woo-experts.local/%d8%b3%d9%84%d8%a7%d9%85-%d8%af%d9%86%db%8c%d8%a7/" rel="bookmark"><time class="entry-date published updated" datetime="2018-05-16T15:51:01+00:00">می 16, 2018</time></a></span><span class="cat-links"><span class="screen-reader-text">دسته‌ها </span><a href="http://woo-experts.local/category/%d8%af%d8%b3%d8%aa%d9%87%e2%80%8c%d8%a8%d9%86%d8%af%db%8c-%d9%86%d8%b4%d8%af%d9%87/" rel="category tag">دسته‌بندی نشده</a></span><span class="comments-link"><a href="http://woo-experts.local/%d8%b3%d9%84%d8%a7%d9%85-%d8%af%d9%86%db%8c%d8%a7/#comments">۱ دیدگاه <span class="screen-reader-text">برای سلام دنیا!</span></a></span>		<span class="edit-link"><a class="post-edit-link" href="http://woo-experts.local/wp-admin/post.php?post=1&#038;action=edit">ویرایش</a></span>	</footer><!-- .entry-footer -->

</article><!-- #post-## -->

		</main><!-- .site-main -->
	</div><!-- .content-area -->


	</div><!-- .site-content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
									<a href="http://wp-persian.com/" class="imprint">
				با افتخار نیرو گرفته از WordPress			</a>
		</div><!-- .site-info -->
	</footer><!-- .site-footer -->

</div><!-- .site -->

<script type='text/javascript' src='http://woo-experts.local/wp-includes/js/admin-bar.min.js?ver=4.9.6'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"http:\/\/woo-experts.local\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"\u0644\u0637\u0641\u0627 \u062a\u0627\u06cc\u06cc\u062f \u0646\u0645\u0627\u06cc\u06cc\u062f \u06a9\u0647 \u0634\u0645\u0627 \u06cc\u06a9 \u0631\u0628\u0627\u062a \u0646\u06cc\u0633\u062a\u06cc\u062f."}},"cached":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='http://woo-experts.local/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.0.2'></script>
<script type='text/javascript' src='http://woo-experts.local/wp-content/themes/twentyfifteen/js/skip-link-focus-fix.js?ver=20141010'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var screenReaderText = {"expand":"<span class=\"screen-reader-text\">\u0628\u0627\u0632\u06a9\u0631\u062f\u0646 \u0632\u06cc\u0631\u0641\u0647\u0631\u0633\u062a<\/span>","collapse":"<span class=\"screen-reader-text\">\u0628\u0633\u062a\u0646 \u0632\u06cc\u0631\u0641\u0647\u0631\u0633\u062a<\/span>"};
/* ]]> */
</script>
<script type='text/javascript' src='http://woo-experts.local/wp-content/themes/twentyfifteen/js/functions.js?ver=20150330'></script>
<script type='text/javascript' src='http://woo-experts.local/wp-includes/js/wp-embed.min.js?ver=4.9.6'></script>
	<!--[if lte IE 8]>
		<script type="text/javascript">
			document.body.className = document.body.className.replace( /(^|\s)(no-)?customize-support(?=\s|$)/, '' ) + ' no-customize-support';
		</script>
	<![endif]-->
	<!--[if gte IE 9]><!-->
		<script type="text/javascript">
			(function() {
				var request, b = document.body, c = 'className', cs = 'customize-support', rcs = new RegExp('(^|\\s+)(no-)?'+cs+'(\\s+|$)');

						request = true;
		
				b[c] = b[c].replace( rcs, ' ' );
				// The customizer requires postMessage and CORS (if the site is cross domain)
				b[c] += ( window.postMessage && request ? ' ' : ' no-' ) + cs;
			}());
		</script>
	<!--<![endif]-->
			<div id="wpadminbar" class="nojq nojs">
							<a class="screen-reader-shortcut" href="#wp-toolbar" tabindex="1">رفتن به نوارابزار</a>
						<div class="quicklinks" id="wp-toolbar" role="navigation" aria-label="نوار ابزار" tabindex="0">
				<ul id="wp-admin-bar-root-default" class="ab-top-menu">
		<li id="wp-admin-bar-wp-logo" class="menupop"><a class="ab-item" aria-haspopup="true" href="http://woo-experts.local/wp-admin/about.php"><span class="ab-icon"></span><span class="screen-reader-text">درباره وردپرس</span></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-wp-logo-default" class="ab-submenu">
		<li id="wp-admin-bar-about"><a class="ab-item" href="http://woo-experts.local/wp-admin/about.php">درباره وردپرس</a>		</li></ul><ul id="wp-admin-bar-wp-logo-external" class="ab-sub-secondary ab-submenu">
		<li id="wp-admin-bar-wporg"><a class="ab-item" href="https://wordpress.org/">وردپرس</a>		</li>
		<li id="wp-admin-bar-documentation"><a class="ab-item" href="https://codex.wordpress.org/">مستندات</a>		</li>
		<li id="wp-admin-bar-support-forums"><a class="ab-item" href="https://wordpress.org/support/">انجمن‌های پشتیبانی</a>		</li>
		<li id="wp-admin-bar-feedback"><a class="ab-item" href="https://wordpress.org/support/forum/requests-and-feedback">بازخورد</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-site-name" class="menupop"><a class="ab-item" aria-haspopup="true" href="http://woo-experts.local/wp-admin/">دوره متخصص وردپرس و ووکامرس سون لرن</a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-site-name-default" class="ab-submenu">
		<li id="wp-admin-bar-dashboard"><a class="ab-item" href="http://woo-experts.local/wp-admin/">پیشخوان</a>		</li></ul><ul id="wp-admin-bar-appearance" class="ab-submenu">
		<li id="wp-admin-bar-themes"><a class="ab-item" href="http://woo-experts.local/wp-admin/themes.php">پوسته‌ها</a>		</li>
		<li id="wp-admin-bar-widgets"><a class="ab-item" href="http://woo-experts.local/wp-admin/widgets.php">ابزارک‌ها </a>		</li>
		<li id="wp-admin-bar-menus"><a class="ab-item" href="http://woo-experts.local/wp-admin/nav-menus.php">فهرست‌ها</a>		</li>
		<li id="wp-admin-bar-background" class="hide-if-customize"><a class="ab-item" href="http://woo-experts.local/wp-admin/themes.php?page=custom-background">پس‌زمینه</a>		</li>
		<li id="wp-admin-bar-header" class="hide-if-customize"><a class="ab-item" href="http://woo-experts.local/wp-admin/themes.php?page=custom-header">سربرگ</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-customize" class="hide-if-no-customize"><a class="ab-item" href="http://woo-experts.local/wp-admin/customize.php?url=http%3A%2F%2Fwoo-experts.local%2F">سفارشی‌سازی</a>		</li>
		<li id="wp-admin-bar-comments"><a class="ab-item" href="http://woo-experts.local/wp-admin/edit-comments.php"><span class="ab-icon"></span><span class="ab-label awaiting-mod pending-count count-0" aria-hidden="true">0</span><span class="screen-reader-text">0 دیدگاه در انتظار مدیریت است</span></a>		</li>
		<li id="wp-admin-bar-new-content" class="menupop"><a class="ab-item" aria-haspopup="true" href="http://woo-experts.local/wp-admin/post-new.php"><span class="ab-icon"></span><span class="ab-label">تازه</span></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-new-content-default" class="ab-submenu">
		<li id="wp-admin-bar-new-post"><a class="ab-item" href="http://woo-experts.local/wp-admin/post-new.php">نوشته</a>		</li>
		<li id="wp-admin-bar-new-media"><a class="ab-item" href="http://woo-experts.local/wp-admin/media-new.php">رسانه</a>		</li>
		<li id="wp-admin-bar-new-page"><a class="ab-item" href="http://woo-experts.local/wp-admin/post-new.php?post_type=page">برگه</a>		</li>
		<li id="wp-admin-bar-new-user"><a class="ab-item" href="http://woo-experts.local/wp-admin/user-new.php">کاربر</a>		</li></ul></div>		</li></ul><ul id="wp-admin-bar-top-secondary" class="ab-top-secondary ab-top-menu">
		<li id="wp-admin-bar-search" class="admin-bar-search"><div class="ab-item ab-empty-item" tabindex="-1"><form action="http://woo-experts.local/" method="get" id="adminbarsearch"><input class="adminbar-input" name="s" id="adminbar-search" type="text" value="" maxlength="150" /><label for="adminbar-search" class="screen-reader-text">جست‌وجو</label><input type="submit" class="adminbar-button" value="جست‌وجو"/></form></div>		</li>
		<li id="wp-admin-bar-my-account" class="menupop with-avatar"><a class="ab-item" aria-haspopup="true" href="http://woo-experts.local/wp-admin/profile.php">سلام <span class="display-name">alimohammadi</span><img alt='' src='http://2.gravatar.com/avatar/2ef82a6ef1aaa53339a8b78e58126f29?s=26&#038;d=mm&#038;r=g' srcset='http://2.gravatar.com/avatar/2ef82a6ef1aaa53339a8b78e58126f29?s=52&#038;d=mm&#038;r=g 2x' class='avatar avatar-26 photo' height='26' width='26' /></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-user-actions" class="ab-submenu">
		<li id="wp-admin-bar-user-info"><a class="ab-item" tabindex="-1" href="http://woo-experts.local/wp-admin/profile.php"><img alt='' src='http://2.gravatar.com/avatar/2ef82a6ef1aaa53339a8b78e58126f29?s=64&#038;d=mm&#038;r=g' srcset='http://2.gravatar.com/avatar/2ef82a6ef1aaa53339a8b78e58126f29?s=128&#038;d=mm&#038;r=g 2x' class='avatar avatar-64 photo' height='64' width='64' /><span class='display-name'>alimohammadi</span></a>		</li>
		<li id="wp-admin-bar-edit-profile"><a class="ab-item" href="http://woo-experts.local/wp-admin/profile.php">ویرایش شناسنامه‌ی من</a>		</li>
		<li id="wp-admin-bar-logout"><a class="ab-item" href="http://woo-experts.local/wp-login.php?action=logout&#038;_wpnonce=32ade9493a">بیرون رفتن</a>		</li></ul></div>		</li></ul>			</div>
						<a class="screen-reader-shortcut" href="http://woo-experts.local/wp-login.php?action=logout&#038;_wpnonce=32ade9493a">بیرون رفتن</a>
					</div>

		
</body>
</html>

<!-- Dynamic page generated in 0.417 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2018-05-27 14:06:38 -->
